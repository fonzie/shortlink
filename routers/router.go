package routers

import (
	"github.com/gin-gonic/gin"
	"shortlink/pkg/setting"
	"shortlink/routers/api"
)

func InitRouter() *gin.Engine {
	// 初始化gin对象
	routers := gin.New()

	// 使用中间件
	routers.Use(gin.Logger())
	routers.Use(gin.Recovery())

	shortlinkTool := routers.Group("/shortlink")
	{
		// 获取指定短链接的信息
		shortlinkTool.GET(setting.AppSetting.ShortLinkPrefix+"/*path", api.GetShortLink)
		// 生成短链接
		shortlinkTool.POST("/", api.PostShortLink)
	}

	// 接收短链接并验证,重定向302
	shortlink := routers.Group(setting.AppSetting.ShortLinkPrefix)
	{
		shortlink.GET("/*path", api.RedirectShortLink)
	}

	routers.GET("/health", api.HealthCheck)

	return routers
}
