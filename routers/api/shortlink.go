package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"shortlink/pkg/app"
	"shortlink/pkg/e"
	"shortlink/pkg/setting"
	"shortlink/service/shortlink"
	"strings"
)

type GetShortLinkFrom struct {
}

// 通过短链接获取长链接
func GetShortLink(c *gin.Context) {
	appG := app.Gin{
		C: c,
	}

	sUri := strings.Replace(c.Param("path"), "/", "", 1)
	shortLinkService := shortlink.ShortLinkService{
		SUrl: sUri,
	}

	err := shortLinkService.GetLongLink()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"longUri":  shortLinkService.LUrl,
		"shortUri": setting.AppSetting.ShortLinkPrefix + "/" + shortLinkService.SUrl,
	})

}

type PostShortLinkFrom struct {
	Uri     string `form:"url" valid:"Required;MinSize(1);MaxSize(65535)"`
	Timeout int    `form:"timeout" valid:"Required;Min(-1)"`
}

// 通过长链接获取短链接
func PostShortLink(c *gin.Context) {
	var (
		appG = app.Gin{
			C: c,
		}
		form PostShortLinkFrom
	)

	// 绑定表单到结构体
	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}

	// 获取短链接
	shortLinkService := shortlink.ShortLinkService{
		LUrl:    form.Uri,
		Timeout: form.Timeout,
	}
	err := shortLinkService.ShortLinkFounder()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.GET_SHORT_LINK_ERROR, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"shortLink": fmt.Sprintf("%s/%s", setting.AppSetting.ShortLinkPrefix, shortLinkService.SUrl),
		"timeout":   shortLinkService.Timeout,
	})

}

type RedirectShortLinkFrom struct {
}

// 302重定向短链接到链接
func RedirectShortLink(c *gin.Context) {
	var (
		appG = app.Gin{
			C: c,
		}
	)

	sUri := strings.Replace(c.Param("path"), "/", "", 1)

	shortLinkService := shortlink.ShortLinkService{
		SUrl: sUri,
	}
	err := shortLinkService.GetLongLink()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.GET_LONG_LINK_ERROR, nil)
		return
	}
	// 统计短链接访问次数
	shortLinkService.ShortLinkAccessCount()

	appG.C.Redirect(302, shortLinkService.LUrl)

}
