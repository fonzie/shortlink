package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"shortlink/pkg/app"
	"shortlink/pkg/e"
	"shortlink/service/healthCheck"
)

func HealthCheck(c *gin.Context) {
	appG := app.Gin{
		C: c,
	}

	healthCheckStatus := &healthCheck.HealthCheck{}
	err := healthCheckStatus.HealthStatusCheck()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
		return
	}

	//healthCheckStatusBytes, err := json.Marshal(healthCheckStatus)
	//if err != nil {
	//	appG.Response(http.StatusInternalServerError, e.ERROR, err.Error())
	//	return
	//}
	appG.C.JSON(http.StatusOK, healthCheckStatus)
}
