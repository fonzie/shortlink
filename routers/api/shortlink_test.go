package api

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"testing"
	"time"
)

func TestPostShortLink(t *testing.T) {
	rand.Seed(time.Now().UnixNano())

	for i := 0; i <= 9223372036854775807; i++ {
		t.Logf("第 %d 次请求", i)

		resp, err := http.Post(fmt.Sprintf("http://127.0.0.1:8080/shortlink?url=https://www.fonzie/%d&timeout=-1", i), "none", nil)
		if err != nil {
			t.Logf("发起http请求失败: %v", err)
			time.Sleep(time.Second * 100)
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Logf("获取body失败")
			time.Sleep(time.Second * 100)
		}

		t.Logf(string(body))
		resp.Body.Close()
	}
}
