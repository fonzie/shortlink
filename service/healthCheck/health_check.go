package healthCheck

import (
	"fmt"
	"net"
	"shortlink/pkg/gredis"
	"shortlink/pkg/logging"
	"shortlink/pkg/setting"
)

type HealthCheck struct {
	Status   string `json:"status"`
	Address  string `json:"address"`
	Version  string `json:"version"`
	AppName  string `json:"appName"`
	RunEnv   string `json:"runEnv"`
	RunModel string `json:"runModel"`
}

func (h *HealthCheck) HealthStatusCheck() error {
	ipAddress := h.getIntranetIP()
	redisHealth := h.redisCheck()

	h.Address = ipAddress
	h.AppName = "ShortLink"
	h.Version = "latest"
	h.Status = "Up"
	h.RunModel = setting.ServerSetting.RunMode
	if setting.ServerSetting.RunMode == "release" {
		h.RunEnv = "prod"
	} else {
		h.RunEnv = "dev"
	}

	if redisHealth != nil {
		h.Status = "Down"
		return redisHealth
	}

	return nil

}

func (h *HealthCheck) getIntranetIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}

	for _, addr := range addrs {
		if ipNet, ok := addr.(*net.IPNet); ok && !ipNet.IP.IsLoopback() {
			if ipNet.IP.To4() != nil {
				return string(ipNet.IP.String())
			}
		}
	}
	return ""
}

// Redis健康检查
func (h *HealthCheck) redisCheck() error {
	err := gredis.HealthCheck()
	if err != nil {
		logging.Error(fmt.Sprintf("Redis health check is not pass, err: %v", err))
	}
	return err
}
