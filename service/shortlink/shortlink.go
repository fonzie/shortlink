package shortlink

import (
	"encoding/json"
	"errors"
	"fmt"
	"shortlink/pkg/gredis"
	"shortlink/pkg/logging"
	"shortlink/pkg/util"
)

const (
	shortUriConst        = "shortUri"
	longUriConst         = "longUri"
	linkTimeout          = "timeout"
	shortLink            = "shortLink:%s"
	shortLinkAutoID      = "shortLinkAutoID"
	longLinkMd5          = "longLinkMd5:%s"
	shortLinkAccessCount = "shortLinkAccessCount:%s"
)

type ShortLinkService struct {
	LUrl        string // 长链接URL
	SUrl        string // 短链接URL
	LUrlIsExist bool   // 长链接是否已经存在Redis中
	EncodeStr   string // 长链接的md5值
	Timeout     int    // 过期时间s
}

// 判断长链接是否已经存储在Redis中了
func (s *ShortLinkService) IsExist() (bool) {
	encodeStr := util.EncodeMd5(s.LUrl)
	s.EncodeStr = encodeStr

	// 判断当前长链接是否已经存在redis中
	urlExist := gredis.Exists(fmt.Sprintf(longLinkMd5, encodeStr))
	s.LUrlIsExist = urlExist
	if urlExist {
		return true
	}
	return false
}

// 查询redis,通过md5获取短链接获取短链接
func (s *ShortLinkService) GetShortLink() (error) {
	linkInfoByte, err := gredis.Get(fmt.Sprintf(longLinkMd5, s.EncodeStr))
	if err != nil {
		return err
	}

	linkInfo := make(map[string]interface{})
	err = json.Unmarshal(linkInfoByte, &linkInfo)
	if err != nil {
		return err
	}

	if v, ok := linkInfo["shortUri"]; ok {
		s.SUrl = v.(string)
	} else {
		return errors.New(fmt.Sprintf("By redis get short link failure, Key does not exist"))
	}

	if v, ok := linkInfo["longUri"]; ok {
		s.LUrl = v.(string)
	}

	return nil
}

// 创建短链接
func (s *ShortLinkService) CreateShortLink() (error) {
	linkInfo := make(map[string]interface{})

	encodeStr := util.EncodeMd5(s.LUrl)
	autoId, err := gredis.Incr(fmt.Sprintf(shortLinkAutoID), s.Timeout)
	if err != nil {
		return err
	}

	shortUri := util.Trans10To62(autoId)

	// 如果短链接已经存在
	if exist := gredis.Exists(fmt.Sprintf(shortLink, shortUri)); exist {
		logging.Warn("短链接重复")
	}

	linkInfo[shortUriConst] = shortUri
	linkInfo[longUriConst] = s.LUrl
	linkInfo[linkTimeout] = s.Timeout

	s.SUrl = shortUri

	// 保存key value - longLinkMd5:<md5> {shortUri: <shortUri>, longUri: <longUri>}
	err = gredis.Set(fmt.Sprintf(longLinkMd5, encodeStr), linkInfo, s.Timeout)
	if err != nil {
		return errors.New(fmt.Sprintf("Save long link key md5 and value map to redis failure, err: %v", err))
	}

	// 保存key value - shortLink:<shortUri> longLinkMd5:<md5>
	err = gredis.Set(fmt.Sprintf(shortLink, shortUri), encodeStr, s.Timeout)
	if err != nil {
		return errors.New(fmt.Sprintf("Save short link key and value md5 to redis failure, err: %v", err))
	}

	return nil
}

// 整合通过长链接获取短链接的逻辑
func (s *ShortLinkService) ShortLinkFounder() (err error) {
	shortLinkIsExist := s.IsExist()

	if shortLinkIsExist {
		err = s.GetShortLink()
		if err != nil {
			logging.Error(fmt.Sprintf("通过已经存在的md5值获取短链接失败..... err: %v", err))
			return err
		}
		return nil
	}

	err = s.CreateShortLink()
	if err != nil {
		logging.Error(fmt.Sprintf("创建短链接发生错误: %v", err))
		return err
	}

	return nil
}

// 通过短链接获取长链接
func (s *ShortLinkService) GetLongLink() (error) {
	// 判断redis中是否存在短链接
	if exist := gredis.Exists(fmt.Sprintf(shortLink, s.SUrl)); !exist {
		return errors.New("系统中未找到对应的短链接....")
	}

	// 通过短链接获取长链接md5值
	longLinkMd5Byte, err := gredis.Get(fmt.Sprintf(shortLink, s.SUrl))
	if err != nil {
		logging.Error(fmt.Sprintf("通过短链接获取长链接的md5值发生错误, err : %v", err))
		return err
	}

	// 反序列化获取到的长链接md5值
	var longLinkMd5String string
	err = json.Unmarshal(longLinkMd5Byte, &longLinkMd5String)
	if err != nil {
		logging.Error(fmt.Sprintf("Unmarshal redis data failurl, err : %v", err))
		return err
	}

	// 使用长链接md5 获取完整的长链接和断链的json数据
	shortLinkInfoByte, err := gredis.Get(fmt.Sprintf(longLinkMd5, longLinkMd5String))
	if err != nil {
		logging.Error(fmt.Sprintf("使用长链接的md5 获取完整的长链接和短链接的"))
		return err
	}

	// 反序列化获取的json数据
	shortLinkInfoMap := make(map[string]interface{})
	err = json.Unmarshal(shortLinkInfoByte, &shortLinkInfoMap)
	if err != nil {
		logging.Error(fmt.Sprintf("反序列化短链接信息发生错误: %v", err))
		return err
	}

	l, ok := shortLinkInfoMap[longUriConst]
	if ok {
		s.LUrl = l.(string)
	}

	return nil
}

// 短链接访问次数统计
func (s *ShortLinkService) ShortLinkAccessCount() {
	var accessCountInt int64

	if exist := gredis.Exists(fmt.Sprintf(shortLinkAccessCount, s.SUrl)); exist {
		accessCountByte, err := gredis.Get(fmt.Sprintf(shortLinkAccessCount, s.SUrl))
		if err != nil {
			logging.Error(fmt.Sprintf("从Redis中获取短链接: %s 计数值发生错误", s.SUrl))
			return
		}

		err = json.Unmarshal(accessCountByte, &accessCountInt)
		if err != nil {
			logging.Error(fmt.Sprintf("反序列化短链接: %s 计数值发生错误", s.SUrl))
			return
		}
		accessCountInt += 1
		err = gredis.Set(fmt.Sprintf(shortLinkAccessCount, s.SUrl), accessCountInt, -1)
		if err != nil {
			logging.Error(fmt.Sprintf("短链接: %s 更新计数值发生错误", s.SUrl))
			return
		}
		logging.Info(fmt.Sprintf("短链接: %s 更新计数值成功!!!", s.SUrl))
		return
	} else {
		accessCountInt += 1
		err := gredis.Set(fmt.Sprintf(shortLinkAccessCount, s.SUrl), accessCountInt, -1)
		if err != nil {
			logging.Error(fmt.Sprintf("插入短链接: %s 计数值发生错误..", s.SUrl))
		}
		return
	}
}
