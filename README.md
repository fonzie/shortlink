# shortlink

#### 介绍
短链接服务,项目使用golang的gin框架编写

#### 安装教程
一般只需要配置```config/app.ini```中的redis配置就可以了其他的默认就可以了
```
[app]
ShortLinkPrefix = /r


# Redis配置
[redis]
Host = 10.0.7.103
Port = 6379
Passowrd = 123456
MaxIdle = 30
MaxActive = 30
IdleTimeout = 200

# Http服务器配置
[server]
# set debug or release
RunMode = debug
HttpPort = 8081
ReadTimeout = 60
WriteTimeout = 60
```

运行服务
```
go run shortlink.go
```

看到如下信息表示运行成功

```
[GIN-debug] [WARNING] Running in "debug" mode. Switch to "release" mode in production.
 - using env:   export GIN_MODE=release
 - using code:  gin.SetMode(gin.ReleaseMode)

[GIN-debug] GET    /shortlink/r/*path        --> shortlink/routers/api.GetShortLink (3 handlers)
[GIN-debug] POST   /shortlink/               --> shortlink/routers/api.PostShortLink (3 handlers)
[GIN-debug] GET    /r/*path                  --> shortlink/routers/api.RedirectShortLink (3 handlers)
[INFO][shortlink.go:43]2019/08/28 17:31:36 [Start http server listening :8081]
[INFO][shortlink.go:44]2019/08/28 17:31:36 [You can access http://0.0.0.0::8081]

```

#### 使用方法

生成短链接
```bash
curl -X POST http://127.0.0.1:8081/shortlink/\?url\=https://gitee.com/fonzie/shortlink\&timeout\=-1
```

这里会返回短链接信息:
```json
{
    "code":200,
    "msg":"ok",
    "data":{
        "shortLink":"/r/c",
        "timeout":-1
    }
}
```


通过短链接获取长链接
```
curl 127.0.0.1:8081/shortlink/r/c
```

返回json信息
```json
{
    "code":200,
    "msg":"ok",
    "data":{
        "longUri":"https://gitee.com/fonzie/shortlink",
        "shortUri":"/r/c"
    }
}
```

访问短链接 302跳转到长链接<说明：这里用302跳转是为实现一个计数的功能>
```bash
curl   127.0.0.1:8081/r/c
```

返回redirect的域名:

```
<a href="https://gitee.com/fonzie/shortlink">Found</a>
```


