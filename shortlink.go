package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"shortlink/pkg/gredis"
	"shortlink/pkg/logging"
	"shortlink/pkg/setting"
	"shortlink/routers"
)

func init() {
	var err error
	setting.Setup()
	logging.Setup()
	err = gredis.Setup()
	if err != nil {
		log.Fatalf("[ERROR] Redis setup err: %v", err)
	}

}

func serviceListenAndServer() {
	gin.SetMode(setting.ServerSetting.RunMode)

	routersInit := routers.InitRouter()
	readTimeout := setting.ServerSetting.ReadTimeout
	writeTimeout := setting.ServerSetting.WriteTimeout

	endPoint := fmt.Sprintf(":%s", setting.ServerSetting.HttpPort)
	maxHeaderBytes := 1 << 20

	server := &http.Server{
		Addr:           endPoint,
		Handler:        routersInit,
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}

	logging.Info(fmt.Sprintf("Start http server listening %s", endPoint))
	logging.Info(fmt.Sprintf("You can access http://0.0.0.0:%s", endPoint))
	logging.Error(server.ListenAndServe())
}

func main() {
	serviceListenAndServer()
}


