package setting

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"testing"
)

type Config struct {
	App    *App
	Redis  *Redis
	Server *Server
}

func TestPrintConfig(t *testing.T) {
	configPath = "../../config/app.ini"
	fmt.Println("打印配置文件:")
	Setup()

	config := &Config{
		App:    AppSetting,
		Redis:  RedisSetting,
		Server: ServerSetting,
	}

	configYaml, err := yaml.Marshal(&config)
	if err != nil {
		t.Fatalf("Yaml marshal config err : %v", err)
	}

	fmt.Println(string(configYaml))

}
