package setting

import (
	"github.com/go-ini/ini"
	"log"
	"time"
)

// APP 配置,如果config/config.ini有添加相关的配置只需要在这个结构体中添加即可
type App struct {
	ShortLinkPrefix string `ini:"ShortLinkPrefix"`
}

// Server 配置,HTTP和GIN相关的配置
type Server struct {
	RunMode      string        `ini:"RunMode"`
	HttpPort     string        `ini:"HttpPort"`
	ReadTimeout  time.Duration `ini:"ReadTimeout"`
	WriteTimeout time.Duration `ini:"WriteTimeout"`
}

// Redis 配置
type Redis struct {
	Host        string        `ini:"Host"`
	Port        string        `ini:"Port"`
	Password    string        `ini:"Password"`
	MaxIdle     int           `ini:"MaxIdle"`
	MaxActive   int           `ini:"MaxActive"`
	IdleTimeout time.Duration `ini:"IdleTimeout"`
	DB          int           `ini:"DB"`
}

var AppSetting = &App{
	ShortLinkPrefix: "/r",
}

// 默认服务器配置
var ServerSetting = &Server{
	RunMode:      "release",
	HttpPort:     "8080",
	ReadTimeout:  60,
	WriteTimeout: 60,
}

// 默认redis配置
var RedisSetting = &Redis{
	Host:        "127.0.0.1",
	Port:        "6379",
	Password:    "",
	MaxIdle:     30,
	MaxActive:   30,
	IdleTimeout: 200,
	DB:          5,
}

var cfg *ini.File
var configPath string

// 配置文件初始化
func Setup() {
	var err error
	if configPath == "" {
		configPath = "config/app.ini"
	}
	cfg, err = ini.Load(configPath)
	if err != nil {
		log.Fatalf("A fatal error occurred in initializing the server configuration: %v", err)
	}

	// 结构体和配置的映射
	mapTo("server", ServerSetting)
	mapTo("gredis", RedisSetting)
	mapTo("app", AppSetting)

	RedisSetting.IdleTimeout = RedisSetting.IdleTimeout * time.Second
	ServerSetting.WriteTimeout = ServerSetting.WriteTimeout * time.Second
	ServerSetting.ReadTimeout = ServerSetting.ReadTimeout * time.Second
}

func mapTo(section string, v interface{}) {
	err := cfg.Section(section).MapTo(v)
	if err != nil {
		log.Fatalf("Cfg.mapTo %s err : %v", section, v)
	}
}
