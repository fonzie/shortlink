package app

import (
	"github.com/astaxie/beego/validation"
	"shortlink/pkg/logging"
)

func MarkErrors(errors []*validation.Error) {
	for _, err := range errors {
		logging.Error(err.Key, err.Message)
	}
	return
}
