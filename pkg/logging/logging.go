package logging

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
)

type Level int

var (
	DefaultPrefix      = ""
	DefaultCallerDepth = 2

	logger     *log.Logger
	logPrefix  = ""
	levelFlags = []string{"DEBUG", "INFO", "WARN", "ERROR", "FATAL"}
)

const (
	debug Level = iota
	info
	warning
	e
	fatal
)

// 初始化日志配置
func Setup() {
	logAndStdout := io.MultiWriter([]io.Writer{
		os.Stdout,
	}...)

	logger = log.New(logAndStdout, DefaultPrefix, log.LstdFlags)
}

func Debug(v ...interface{}) {
	setPrefix(debug)
	logger.Println(v)
}

func Info(v ...interface{}) {
	setPrefix(info)
	logger.Println(v)
}

func Warn(v ...interface{}) {
	setPrefix(warning)
	logger.Println(v)
}

func Error(v ...interface{}) {
	setPrefix(e)
	logger.Println(v)
}

func Fatal(v ...interface{}) {
	setPrefix(fatal)
	logger.Println(v)
}

func setPrefix(level Level) {
	_, file, line, ok := runtime.Caller(DefaultCallerDepth)
	if ok {
		logPrefix = fmt.Sprintf("[%s][%s:%d]", levelFlags[level], filepath.Base(file), line)
	} else {
		logPrefix = fmt.Sprintf("[%s]", levelFlags[level])
	}
	logger.SetPrefix(logPrefix)
}
