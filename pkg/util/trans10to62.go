package util

// 十进制转成62进制 0-9a-zA-Z 六十二进制
func Trans10To62(id int64) (string) {
	// 1 -- > b
	// 10-- > 1
	// 61-- > Z
	charset := "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	var shortUrl []byte

	for {
		var result byte
		var tmp []byte
		number := id % 62

		result = charset[number]

		tmp = append(tmp, result)
		shortUrl = append(tmp, shortUrl...)

		id = id / 62
		if id == 0 {
			break
		}
	}
	return string(shortUrl)
}
