package e

var MsgFlags = map[int]string{
	SUCCESS:              "ok",
	ERROR:                "fail",
	INVALID_PARAMS:       "请求参数错误",
	GET_SHORT_LINK_ERROR: "获取短链接发生错误",
	GET_LONG_LINK_ERROR: "获取长链接发生错误",
}

// 获取返回code码
func GetMsg(code int) string {
	msg, ok := MsgFlags[code]
	if ok {
		return msg
	}

	return MsgFlags[ERROR]
}
