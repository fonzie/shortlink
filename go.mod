module shortlink

go 1.12

require (
	github.com/astaxie/beego v1.12.0
	github.com/gin-gonic/gin v1.4.0
	github.com/go-ini/ini v1.46.0
	github.com/gomodule/redigo v2.0.0+incompatible
	gopkg.in/yaml.v2 v2.2.2
)
